//WAP to find the sum of n fractions.
#include<stdio.h>
typedef struct fraction
{
    int n;
    int d;
}frac;
int num;
struct fraction input()
{
    frac data;
    printf("Enter the numerator of the number:\n");
    scanf("%d",&data.n);
    printf("Enter the denominator of the number:\n");
    scanf("%d",&data.d);
    return data;
}
int gcd(int a, int b)
{
    if(a==0)
    return b;
    else
    return gcd(b%a,a);
}
int lcm(frac arr[])
{
    int ans;
    ans=arr[0].d;
    for(int i=1;i<num;i++)
    {
        ans=(ans*arr[i].d)/gcd(ans,arr[i].d);
    }
    return ans;
}
struct fraction add(frac arr[],int fd)
{
    frac sum;
    sum.n=0;
    for(int i=0;i<num;i++)
    {
        sum.n=sum.n+arr[i].n*(fd/arr[i].d);
    }
    sum.d=fd;
    return sum;
}
struct fraction simplify(frac sum)
{
    int hcf=gcd(sum.n,sum.d);
    sum.n/=hcf;
    sum.d/=hcf;
    return sum;
}
void display(frac sum)
{
    if(sum.n==0)
    printf("Sum = 0\n");
    else if(sum.n==sum.d)
    printf("Sum = 1\n");
    else if(sum.d==1)
    printf("Sum = %d\n",sum.n);
    else
    printf("Sum = %d/%d\n",sum.n,sum.d);
}
int main()
{
    frac sum;
    printf("Enter the number of Fractions to be entered:\n");
    scanf("%d",&num);
    if(num>=2)
    {
        frac array[num];
        for(int i=0;i<num;i++)
        {
            array[i]=input();
        }
        int fd=lcm(array);
        sum=add(array,fd);
        sum=simplify(sum);
        display(sum);
    }
    else
    {
        printf("Try Again!\n");
    }
    
}