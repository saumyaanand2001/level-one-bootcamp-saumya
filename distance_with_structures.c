//WAP to find the distance between two points using structures and 4 functions.
#include <stdio.h>
#include<math.h>
struct point
{
    float x,y;
};
float input(char ch, int i)
{
    printf("Enter the coordinate %c%d: ",ch,i);
    float data; 
    scanf("%f",&data);
    return data;
}

float distance(struct point m, struct point n)
{
    float distance=sqrt(pow((m.x-n.x),2)+pow((m.y-n.y),2));
    return distance;
}

void display(float distance,struct point m, struct point n)
{
    printf("Distance between (%.2f,%.2f)and (%.2f,%.2f) is %f.", m.x,m.y,n.x,n.y,distance);
}

int main()
{
    struct point m;
    struct point n;
    float d;
    int i=1;
    char ch='x';
    m.x=input(ch,i);
    n.x=input(ch,++i);
    ch='y';
    m.y=input(ch,--i);
    n.y=input(ch,++i);
    d=distance(m,n);
    display(d,m,n);
    return 0;
}
