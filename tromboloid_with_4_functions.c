//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input(char ch)
{
     printf("Enter the %c of tromboloid: ",ch);
    float data; 
    scanf("%f",&data);
    return data;
}

float volume(float h,float b,float d)
{
    float volume=((1/3.0)*((h*d)+d))/b;
    return volume;
}

void display(float v)
{
    printf("volume of tromboloid=%f", v);
}

int main()
{
    float h,d,b,v;
    h=input('h');
    d=input('d');
    b=input('b');
    v=volume(h,d,b);
    display(v);
    return 0;
}
