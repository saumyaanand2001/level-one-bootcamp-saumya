//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int inputNum()
{
    int num; 
    printf("Enter a number: ");
    scanf("%d",&num);
    return num;
}

int number_sum(int num1, int num2)
{
    int sum;
    sum = num1+num2;
    return sum;
}

void display(int num1, int num2, int sum)
{
    printf("Sum of %d + %d is %d\n",num1, num2, sum);
}

int main()
{
    int num1,num2,sum;
    num1=inputNum();
    num2=inputNum();
    sum=number_sum(num1,num2);
    display(num1, num2, sum);
    return 0;
}
