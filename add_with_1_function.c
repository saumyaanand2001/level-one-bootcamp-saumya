//Write a program to add two user input numbers using one function.
#include<stdio.h>
int main()
{
	int num1;
	int num2;
	printf("Enter the first number: ");
	scanf("%d",&num1);
	printf("Enter the second number: ");
	scanf("%d",&num2);
	printf("Sum of %d and %d is %d",num1, num2,(num1+num2));
	return 0;
}
