#include <stdio.h>
#include<math.h>
float input(char ch, int i)
{
    printf("Enter the coordinate %c%d coordinate: ",ch,i);
    float data; 
    scanf("%f",&data);
    return data;
}

float distance(float x1,float y1,float x2,float y2)
{
    float distance=sqrt(pow((x1-x2),2)+pow((y1-y2),2));
    return distance;
}

void display(float distance,float x1,float y1,float x2,float y2)
{
    printf("Distance between (%.2f,%.2f)and (%.2f,%.2f) is %f.", x1,y1,x2,y2,distance);
}

int main()
{
    float x1,y1,x2,y2, d;
    int i=1;
    char  ch='x';
    x1=input(ch,i);
    x2=input(ch,++i);
    ch='y';
    y1=input(ch,--i);
    y2=input(ch,++i);
    d=distance(x1,y1,x2,y2);
    display(d,x1,y1,x2,y2);
    return 0;
}
