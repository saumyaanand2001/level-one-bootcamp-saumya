//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct fraction
{
    int n;
    int d;
}frac;
int input(char ch1[20], char ch2[20])
{
    printf("Enter the %s of the %s fraction: ",ch1,ch2);
    int data;
    scanf("%d",&data);
    return data;
}
int lcm(int a, int b) 
{ 
    return (a / gcd(a, b)) * b;
} 
struct fraction add(frac f1, frac f2,int fd)
{
    frac sum;
    f1.n=f1.n*(fd/f1.d);
    f2.n=f2.n*(fd/f2.d);
    sum.n=f1.n+f2.n;
    sum.d=fd;
    return sum;
}
int gcd(int a, int b)
{
    if (b == 0)
        return a;
    return gcd(b, a % b);
}
struct fraction simplify(frac f3)
{
    int hcf=gcd(f3.n,f3.d);
    f3.n/=hcf;
    f3.d/=hcf;
    return f3;
}
void display(frac f1, frac f2,frac sum)
{
    if(sum.n==0)
    printf("Sum of %d/%d and %d/%d is equal to 0",f1.n,f1.d,f2.n,f2.d);
    else if(sum.n==sum.d)
    printf("Sum of %d/%d and %d/%d is equal to 1",f1.n,f1.d,f2.n,f2.d);
    else
    printf("Sum of %d/%d and %d/%d is equal to %d/%d",f1.n,f1.d,f2.n,f2.d, sum.n, sum.d);
}
int main()
{
   frac f1,f2,sum;
   f1.n=input("Numerator","first");
   f1.d=input("Denominator","first");
   f2.n=input("Numerator","second");
   f2.d=input("Denominator","second");
   int fd=lcm(f1.d,f2.d);
   sum=add(f1,f2,fd);
   sum=simplify(sum);
   display(f1,f2,sum);
}