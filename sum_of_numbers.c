//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input()
{
    int x; 
    scanf("%d", &x);
    return x;
}

int add(int a, int b)
{
    int sum;
    sum = a + b;
    return sum;
}

void output(int s)
{
    printf("Sum is %d\n",s);
}

int main() {
	int n, num, sum=0;
	printf("Enter the number of numbers: ");
	scanf("%d", &n);
	printf("Enter the numbers : \n");
	for (int i=0; i<n; i++) {
		num = input();
	    sum = add(sum, num);
	}
	output(sum);
	return 0;
}
